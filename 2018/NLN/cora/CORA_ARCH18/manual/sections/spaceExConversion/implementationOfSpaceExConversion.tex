\label{sx2coradocu}

SX2CORA is a tool that can automatically convert XML SpaceEx models to an equivalent CORA automaton object.

\subsection{Preface: The SpaceEx Format}
SpaceEx model definitions consist of \textit{Network} and \textit{Base Components}.
Base Components describe a monolithic hybrid automaton.
Network Components describe a parallel compositon of $N \in \mathbb{N}$ components.
When a component is \textit{bound} by a Network Component, all variables of the bound component (states, inputs, constant parameters) must be mapped to variables of the binding component or numeric values.
If a component is bound mutiple times, each bind "creates" a new instance of that component with independent variables.
The SpaceEx modeling language is described in greater detail on the SpaceEx website\footnote{http://spaceex.imag.fr/sites/default/files/spaceex\_modeling\_language\_0.pdf}.

\subsection{Parsing The Component Definitions}
The first step of the conversion is parsing and storing all component definitions of the given XML file.

\subsubsection{Accessing XML Files}
We use the popular function \texttt{xml2struct} (Falkena, Wanner, Smirnov) from the MATLAB File Exchange, to conveniently analyse XML files.
The function converts XML structures such as \dots

\begin{lstlisting}
<mynode id="1" note="foobar">
	<foo>FOO</foo>
	<bar>BAR</bar>
</mynode> 
\end{lstlisting}

\dots to a nested MATLAB struct, formatted like this \dots

\begin{nestedlist}
  \textit{MATLAB struct}
  \begin{nestedlist}
    mynode
    \begin{nestedlist}
      Attributes
      \begin{nestedlist}
        id: \textit{'1'}\\
        description: \textit{'foobar'}
      \end{nestedlist}
      foo
      \begin{nestedlist}
        Text: \textit{'FOO'}
      \end{nestedlist}
      bar
      \begin{nestedlist}
        Text: \textit{'BAR'}
      \end{nestedlist}
    \end{nestedlist}
  \end{nestedlist}
\end{nestedlist}

The format allows for intuitive access to attributes, and easy extraction of subnodes.
This lets us simplify our parser code by parsing subnodes in subordinate functions.

\subsubsection{Storing The Component Definitions}
Before we begin semantic evaluation, component definitions are parsed into a more convenient format.
Relevant information is extracted and reformatted, and the component format is partially restructured.

For \textbf{Base Components} we convert the string equations, specifying flow, invariants, guards \& resets, to a more compact and manipulable format.
Furthermore we split the global list of transition to per-locations lists of outgoing transitions.

We use the \textit{Symbolic Math Toolbox} for MATLAB to parse expressions from strings.
It can store arithmetic expressions over variables as \textit{symbolic} expressions.
The Symbolic Math Toolbox provides powerful manipulation tools for symbolic expressions, i.e. variable substitution (\texttt{subs}).

Flow or reset functions are given as a list of equations seperated by ampersands, as in this example from the \textit{platoon\_hybrid} model.

\begin{lstlisting}
<flow>
  x1' == x2 &
  x2' == -x3 + u &
  x3' == 1.605*x1 + 4.868*x2 -3.5754*x3 -0.8198*x4 + 0.427*x5 - 0.045*x6 -
         0.1942*x7 + 0.3626*x8 - 0.0946*x9 &
  x4' == x5 &
  x5' == x3 - x6 &
  x6' == 0.8718*x1 + 3.814*x2 -0.0754*x3 + 1.1936*x4 + 3.6258*x5  -3.2396*x6 -
         0.595*x7+ 0.1294*x8 -0.0796*x9 &
  x7' == x8 &
  x8' == x6 - x9 &
  x9' == 0.7132*x1 + 3.573*x2 - 0.0964*x3 + 0.8472*x4 + 3.2568*x5 - 0.0876*x6 +
         1.2726*x7 + 3.072*x8 - 3.1356*x9 &
  t' == 1
</flow>
\end{lstlisting}

We seperate the equations, and represent each one as a tuple of the l.h.s. variable name (string) and the r.h.s. expression (symbolic).
The result is a struct of the following format (symbolics are indicated by braces):

\begin{nestedlist}
  Flow
  \begin{nestedlist}
    varNames: [ \textit{"x1" "x2" "x3" "x4" "x5" "x6" "x7" "x8" "x9" "t"} ] \\
    expressions: [ \{$x2$\} \{$-x3 + u$\} \dots \{$1$\} ]
  \end{nestedlist}
\end{nestedlist}

Invariant and guard sets are similarly defined by a list of equations or inequations.

\begin{lstlisting}
<invariant>
  t <= 20 &
  min <= u <= max
</invariant>
\end{lstlisting}

We convert both sides of each (in-)equation to symbolics.
The sides are subtracted from each other, to receive an expression that represents the equivalent (in-)equation $expr \leq 0$ resp. $expr = 0$.
The example would then be stored like this:

\begin{nestedlist}
  Invariant
  \begin{nestedlist}
    inequalities: [ \{$t - 20$\} \{$min - u$\} \{$u - max$\} ] \\
    equalities: [ ]
  \end{nestedlist}
\end{nestedlist}

\begin{figure}[h] % packing list in a float, to prevent ugly pagebreak
\begin{nestedlist}
  \textit{Parsed BC template (indexed fields indicate struct arrays):}
  \begin{nestedlist}
    id \\
    \textbf{listOfVar(i)} \\
    \textbf{States(i)}
    \begin{nestedlist}
      name \\
      \textbf{Flow} \\
      \textbf{Invariant} \\
      \textbf{Trans(i)}
      \begin{nestedlist}
        destination \\
        \textbf{guard} \\
        \textbf{reset}
      \end{nestedlist}
    \end{nestedlist}
  \end{nestedlist}
\end{nestedlist}
\end{figure}

For \textbf{Network Components} we need to parse the references to other components, and a variable mapping for each of those.
Variable mappings are stored in both symbolic and string, so they can be efficiently applied to both data types.

Finally we parse the all components' variable definitions and store the needed attributes.
\textit{label}-variables are currently ignored, since synchronisation label logic is not yet implemented.

\begin{figure}[h] % packing list in a float, to prevent ugly pagebreak
\begin{nestedlist}
  \textit{Parsed NC template (indexed fields indicate struct arrays):}
  \begin{nestedlist}
    id \\
    \textbf{listOfVar(i)} \\
    \textbf{Binds(i)}
    \begin{nestedlist}
      id \\
      keys \\
      values \\
      values\_text
    \end{nestedlist}
  \end{nestedlist}
\end{nestedlist}
\end{figure}

While loading models with variables named \textbf{i, j, I or J}, we discovered that our string to symbolic parser (\texttt{str2sym}) automatically replaces them by the constant $\sqrt{-1}$.
As a workaround, we pre-parse all our equations \& variable definitions to rename those variables.
All names fulfilling the regex \texttt{i+|j+|I+|J+} are lengthened by a letter.
The Symbolic Math Toolbox can also substitute other common constants such as \textbf{pi}, but does not do so while parsing.
It is still recommended to avoid them as variable names.

\subsection{"Instantiating" the Component Tree}

At this point we could already generate models from Base Component templates.
To evaluate Network Components however, we need to resolve binds of other components.
We do so, by creating "instances" of bound componets.
Component instances are copies of templates, to which a variable mapping was applied.

The first created instance is an unchanged copy of the root template.
We expand the component tree recursively, creating a new instance for each bind.
This process is repeated until all leaves of the tree are BC instances.

Base Components have variable mappings applied to all of their continuous dynamics fields.
Substitution is used on any symbolic fields (\texttt{expr = subs(expr,keys,values);}).
String fields are updated by dictionary lookup on \texttt{(keys,values\_text)}.

When instantiating a Network Component, the variable mapping is applied to \textit{its} mappings (specifically to \texttt{value} and \texttt{value\_text}).
The result is the concatenation of the two variable mappings.

Once this step is complete, all BC instances use only variables defined in the root component.
We can now generate the SpaceEx model from the parallel composition of all BC instances.

\subsection{Forming the Parallel Composition}

As of this writing, CORA provides only the \texttt{hybridAutomaton} class to store hybrid models.
A prototype of the \texttt{parallelHybridAutomaton} class for efficient storage and simulation of parallel hybrid models is planned for July 2018.
Until then we form the automaton product of parallel models, and convert the result to a \texttt{hybridAutomaton} object.
We implemented a product function for two BC instances, which can be applied iteratively for compositons of more instances.

The product of two instances with discrete state sets $S_1$ and $S_2$ has the state set $S_1 \times S_2$.
Thus we have to compute representation of combined each state $(s1,s2) \in S1 \times S2$.
This means combining flow functions, invariants and transitions.
 
\subsubsection{Combining Flow Functions}
Our representation of flow functions assigns a \textbf{symbolic expression} $sym$ to every state variable name.

\[Flow_x: xNames \rightarrow \mathcal{SYM}(xNames \uplus uNames)\]
\[xNames = \mbox{\textbf{Variable Names} of~} x, uNames = \mbox{\textbf{Variable Vames} of inputs affecting~} \dot{x}\]
\[\mathcal{SYM}(varNames) = \mbox{Set of all \textbf{Symbolic Expressions} over~} varNames\]

Symbolic expressions can in turn be evaluated, given a \textbf{valuation} of the used variables.

\[symeval: \mathcal{SYM}(varNames) \times \mathcal{VAL}(varNames) \rightarrow \mathbb{R}\]
\[\mathcal{VAL}(varNames) = \mbox{Set of all \textbf{Valuations}~} val: varNames \rightarrow \mathbb{R}\]

The combination of two flows $Flow_{x_1},Flow_{x_2}$ should, given any valuation, evaluate all variables to the same value as the orignal function.

\[\forall i \in \{1,2\}, X \in xNames_i, val \in \mathcal{VAL}(xNames_1 \cup xNames_2 \cup uNames_1 \cup uNames_2): \]
\[symeval(Flow_{x_i}(X),val) = symeval(Flow_{x_1 \cup x_2}(X),val)\]

The SpaceEx format ensures that flow functions never have overlapping domains.
Once all variable mappings are applied, the same is true for the sets of state variable names
(though state variables can still appear as input variables of other flows).
Thus a correct combination of two flows is easily realised:

\[Flow_{x_1 \cup x_2}(X) =\left\{ \begin{array}{rcl} 
Flow_{x_1}(X) & \mbox{for} & X \in xNames_1 \\
Flow_{x_2}(X) & \mbox{for} & X \in xNames_2
\end{array}\right. \] 

\subsubsection{Combining Invariants}
Invariant sets are represented by two lists of symbolic expressions, representing inequations and equations.

\[Inv \in \mathcal{SYM}(xNames)^* \times \mathcal{SYM}(xNames)^* \]

A point (equivalent to a \textbf{valuation} here) is contained in the set, if it fulfills all conditions implied by the symbolic expression.

\[contains(Inv,x) = symeval(Inv^1,x) \leq \vec{0} \wedge symeval(Inv^2,x) = \vec{0} \]

For the automaton product, we need to form the intersection of the invariants.
This is equivalent to forming the conjunction of the $contains$ conditions.

\[contains(Inv_{1,2},x) = contains(Inv_1,x\downarrow_{xNames_1}) \wedge contains(Inv_2,x\downarrow_{xNames_2})\]
\[val\downarrow_{Vars}(X)) = \left\{ \begin{array}{rcl} 
val(X) & \mbox{for} & X \in Vars \\
undefined & \mbox{otherwise} & \\
\end{array}\right. \]

We realise the conjunction of the conditions by concatenating the symbolic lists.

\[Inv_{1,2} = (Inv_1^1 \parallel Inv_2^1,Inv_1^2 \parallel Inv_2^2) \]

\subsubsection{Combining Transitions}
We combine the transitions by an \textit{asynchronous} product, i.e. we do not care about possibly simultaneous transitions.
For contrast: in a \textit{synchronous} product the cases of taking only one, only the other, or both transitions would be treated as seperate transitions.
This should not affect the model, since two \textit{simultaneously active} transitions could still be taken in direct succession.
However, this assumption only holds if all component's reset functions and guards only affect/are affected by their component's state variables.

For each state $s_i \in S_i$ a set of outgoing transitions is stored. Each transition has a destination, guard set, and reset function.

\[Trans(s_i) \subseteq S_i \times \mathcal{GUARDS}(xNames_i) \times \mathcal{RESETS}(xNames_i)\]

For a composed state $(s1,s2)$ we collect all outgoing transitions of its component states and generalize them to the bigger variable set.
This projection

From a composed state $(s1,s2)$ outgoing transitions of both $s1$ and $s2$ can be taken.
Since the product automaton has a larger variable domain, guard sets and reset functions need to be expanded, i.e.:

\[(d,guard,reset) \in Trans(s_1) \mapsto ((d,s_2),\widehat{guard},\widehat{reset}) \in Trans((s_1,s_2))\]
\[\forall x \in \mathcal{VAL}(xNames_1 \cup xNames_2):\]
\[contains(\widehat{guard},x) = contains(guard,x\downarrow_{xNames_1})\]
\[\widehat{reset}(x)(X) = \left\{ \begin{array}{rcl}
reset(x\downarrow_{xNames_1})(X) & \mbox{for} & X \in xNames_1 \\
x(X) & \mbox{for} & X \in xNames_2\\
\end{array}\right. \]

Thanks to the symbolic condition representation of guard sets, we do not need to expand them explicitly.
Reset functions are also expanded implicitly, since we assume variables without an assignment to be unchanged (in accordance to the SpaceEx modelling language).

\subsection{Formalizing Symbolic Dynamics}
Once the composed automaton has been created, we need to formalize its continuous dynamics to CORA's numeric format.

\subsubsection{Linear \& Non-linear Flows}
Linear flows are stored in CORA by specifying $A,B,c$ in the equation $\dot{x} = Ax + Bu + c$.
We can obtain these coefficients from the symbolic expressions by computing the partial derivatives.
\[a_{ij} = \frac{\partial Flow(x_i)}{\partial x_j}\]
\[b_{ij} = \frac{\partial Flow(x_i)}{\partial u_j}\]
The $A$ and $B$ matrices are thus computed using the \texttt{jacobian} command from the Symbolic Toolbox.
The constant part $c$ can be obtained by entering $0$ for all variables into the expression.
\[c_i = symeval(Flow(x_i),\langle xNames \cup uNames \mapsto 0\rangle)\]

These computations can also be used to check the linearity of a flow.
$A,B,c$ do not depend on any variables \textbf{iff} the flow is linear.
If a flow fails the linearity test, CORA can still store it in a \texttt{nonLinearSys} object.
This requires the flow computation to be stored in a MATLAB function, which we can easily create by converting the symbolic expressions to strings.

In the same fashion reset functions are evaluated to the format $reset(x) = Ax + b$.
A failiure of the linearity test causes an error here, since CORA does currently not support non-linear reset functions.

\subsubsection{Polyhedric Guard \& Invariant Sets}
The SpaceEx modeling language uses polyhedra for continuous sets.
CORA can store polyhedra with the class \texttt{mptPolytope}, which is based on the \texttt{Polyhedron} class of the Multi-Parametric Toolbox 3 for MATLAB\footnote{http://people.ee.ethz.ch/~mpt/3/}.

They specify polyhedra by the coefficients $A,b,A_e,b_e$ of the equation system $A x \leq b \wedge A_e x = b_e$.
We previously stored guards and invariants as symbolic expressions for $A x - b$ and $A_e x - b_e$.
Analogous to flow, the coefficents are obtained via partial derivatives and insertion of zeros.
Non-linearity causes an error, since only linear sets are supported by CORA.

\subsection{Conversion to MATLAB Function}
Lastly we need a way to convert our parsed automaton specification to a \texttt{hybridAutomaton} MATLAB object.
A MATLAB function is generated, which assembles the object when called.
All numeric parameters (i.e. flow matrices, polyhedra, etc.) are stored in the function as literals.
Additionally a \textit{Interface Specification} comment is added to the header of the file.
It specifies the order, in which the automaton expects its state and input dimensions.

\begin{lstlisting}[language=matlab]
%% Interface Specification:
%   This section clarifies the meaning of state & input dimensions
%   by showing their mapping to SpaceEx variable names. 

% Component 1 (ChaserSpacecraft):
%  state x := [x; y; vx; vy; t]
%  input u := [uDummy]
\end{lstlisting}

It is worth noting, that CORA does currently not support zero-input automata.
As seen in the example above, we add a dummy input without effect.

\subsection{Open Problems}
The SX2CORA converter has reached a baseline of functionality and was already used for CORA's entry to the ARCH Competition 2018.
Its development is not finished however, and is waiting to address several issues, such as:
\begin{itemize}
   \item handling of local variables
   \item flexible \& robust parsing of equations
   \item special handling of constant parameters
   \item parsing of input restrictions by invariants
   \item implementing synchronized composition
\end{itemize}
