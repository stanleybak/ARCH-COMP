function Tf = thirdOrderTensor_dynamicsClosedLoopLinear(x,u)



 Tf{1,1} = interval(sparse(14,14),sparse(14,14));



 Tf{1,2} = interval(sparse(14,14),sparse(14,14));



 Tf{1,3} = interval(sparse(14,14),sparse(14,14));



 Tf{1,4} = interval(sparse(14,14),sparse(14,14));



 Tf{1,5} = interval(sparse(14,14),sparse(14,14));



 Tf{1,6} = interval(sparse(14,14),sparse(14,14));



 Tf{1,7} = interval(sparse(14,14),sparse(14,14));



 Tf{1,8} = interval(sparse(14,14),sparse(14,14));



 Tf{1,9} = interval(sparse(14,14),sparse(14,14));



 Tf{1,10} = interval(sparse(14,14),sparse(14,14));



 Tf{1,11} = interval(sparse(14,14),sparse(14,14));



 Tf{1,12} = interval(sparse(14,14),sparse(14,14));



 Tf{1,13} = interval(sparse(14,14),sparse(14,14));



 Tf{1,14} = interval(sparse(14,14),sparse(14,14));



 Tf{2,1} = interval(sparse(14,14),sparse(14,14));



 Tf{2,2} = interval(sparse(14,14),sparse(14,14));



 Tf{2,3} = interval(sparse(14,14),sparse(14,14));



 Tf{2,4} = interval(sparse(14,14),sparse(14,14));



 Tf{2,5} = interval(sparse(14,14),sparse(14,14));



 Tf{2,6} = interval(sparse(14,14),sparse(14,14));



 Tf{2,7} = interval(sparse(14,14),sparse(14,14));



 Tf{2,8} = interval(sparse(14,14),sparse(14,14));



 Tf{2,9} = interval(sparse(14,14),sparse(14,14));



 Tf{2,10} = interval(sparse(14,14),sparse(14,14));



 Tf{2,11} = interval(sparse(14,14),sparse(14,14));



 Tf{2,12} = interval(sparse(14,14),sparse(14,14));



 Tf{2,13} = interval(sparse(14,14),sparse(14,14));



 Tf{2,14} = interval(sparse(14,14),sparse(14,14));



 Tf{3,1} = interval(sparse(14,14),sparse(14,14));

Tf{3,1}(2,2) = -cos(x(2));


 Tf{3,2} = interval(sparse(14,14),sparse(14,14));

Tf{3,2}(2,1) = -cos(x(2));
Tf{3,2}(1,2) = -cos(x(2));
Tf{3,2}(2,2) = x(1)*sin(x(2));


 Tf{3,3} = interval(sparse(14,14),sparse(14,14));



 Tf{3,4} = interval(sparse(14,14),sparse(14,14));



 Tf{3,5} = interval(sparse(14,14),sparse(14,14));



 Tf{3,6} = interval(sparse(14,14),sparse(14,14));



 Tf{3,7} = interval(sparse(14,14),sparse(14,14));



 Tf{3,8} = interval(sparse(14,14),sparse(14,14));



 Tf{3,9} = interval(sparse(14,14),sparse(14,14));



 Tf{3,10} = interval(sparse(14,14),sparse(14,14));



 Tf{3,11} = interval(sparse(14,14),sparse(14,14));



 Tf{3,12} = interval(sparse(14,14),sparse(14,14));



 Tf{3,13} = interval(sparse(14,14),sparse(14,14));



 Tf{3,14} = interval(sparse(14,14),sparse(14,14));



 Tf{4,1} = interval(sparse(14,14),sparse(14,14));

Tf{4,1}(2,2) = -sin(x(2));


 Tf{4,2} = interval(sparse(14,14),sparse(14,14));

Tf{4,2}(2,1) = -sin(x(2));
Tf{4,2}(1,2) = -sin(x(2));
Tf{4,2}(2,2) = -x(1)*cos(x(2));


 Tf{4,3} = interval(sparse(14,14),sparse(14,14));



 Tf{4,4} = interval(sparse(14,14),sparse(14,14));



 Tf{4,5} = interval(sparse(14,14),sparse(14,14));



 Tf{4,6} = interval(sparse(14,14),sparse(14,14));



 Tf{4,7} = interval(sparse(14,14),sparse(14,14));



 Tf{4,8} = interval(sparse(14,14),sparse(14,14));



 Tf{4,9} = interval(sparse(14,14),sparse(14,14));



 Tf{4,10} = interval(sparse(14,14),sparse(14,14));



 Tf{4,11} = interval(sparse(14,14),sparse(14,14));



 Tf{4,12} = interval(sparse(14,14),sparse(14,14));



 Tf{4,13} = interval(sparse(14,14),sparse(14,14));



 Tf{4,14} = interval(sparse(14,14),sparse(14,14));



 Tf{5,1} = interval(sparse(14,14),sparse(14,14));



 Tf{5,2} = interval(sparse(14,14),sparse(14,14));



 Tf{5,3} = interval(sparse(14,14),sparse(14,14));



 Tf{5,4} = interval(sparse(14,14),sparse(14,14));



 Tf{5,5} = interval(sparse(14,14),sparse(14,14));



 Tf{5,6} = interval(sparse(14,14),sparse(14,14));



 Tf{5,7} = interval(sparse(14,14),sparse(14,14));



 Tf{5,8} = interval(sparse(14,14),sparse(14,14));



 Tf{5,9} = interval(sparse(14,14),sparse(14,14));



 Tf{5,10} = interval(sparse(14,14),sparse(14,14));



 Tf{5,11} = interval(sparse(14,14),sparse(14,14));



 Tf{5,12} = interval(sparse(14,14),sparse(14,14));



 Tf{5,13} = interval(sparse(14,14),sparse(14,14));



 Tf{5,14} = interval(sparse(14,14),sparse(14,14));



 Tf{6,1} = interval(sparse(14,14),sparse(14,14));



 Tf{6,2} = interval(sparse(14,14),sparse(14,14));



 Tf{6,3} = interval(sparse(14,14),sparse(14,14));



 Tf{6,4} = interval(sparse(14,14),sparse(14,14));



 Tf{6,5} = interval(sparse(14,14),sparse(14,14));



 Tf{6,6} = interval(sparse(14,14),sparse(14,14));



 Tf{6,7} = interval(sparse(14,14),sparse(14,14));



 Tf{6,8} = interval(sparse(14,14),sparse(14,14));



 Tf{6,9} = interval(sparse(14,14),sparse(14,14));



 Tf{6,10} = interval(sparse(14,14),sparse(14,14));



 Tf{6,11} = interval(sparse(14,14),sparse(14,14));



 Tf{6,12} = interval(sparse(14,14),sparse(14,14));



 Tf{6,13} = interval(sparse(14,14),sparse(14,14));



 Tf{6,14} = interval(sparse(14,14),sparse(14,14));



 Tf{7,1} = interval(sparse(14,14),sparse(14,14));



 Tf{7,2} = interval(sparse(14,14),sparse(14,14));



 Tf{7,3} = interval(sparse(14,14),sparse(14,14));



 Tf{7,4} = interval(sparse(14,14),sparse(14,14));



 Tf{7,5} = interval(sparse(14,14),sparse(14,14));



 Tf{7,6} = interval(sparse(14,14),sparse(14,14));



 Tf{7,7} = interval(sparse(14,14),sparse(14,14));



 Tf{7,8} = interval(sparse(14,14),sparse(14,14));



 Tf{7,9} = interval(sparse(14,14),sparse(14,14));



 Tf{7,10} = interval(sparse(14,14),sparse(14,14));



 Tf{7,11} = interval(sparse(14,14),sparse(14,14));



 Tf{7,12} = interval(sparse(14,14),sparse(14,14));



 Tf{7,13} = interval(sparse(14,14),sparse(14,14));



 Tf{7,14} = interval(sparse(14,14),sparse(14,14));



 Tf{8,1} = interval(sparse(14,14),sparse(14,14));



 Tf{8,2} = interval(sparse(14,14),sparse(14,14));



 Tf{8,3} = interval(sparse(14,14),sparse(14,14));



 Tf{8,4} = interval(sparse(14,14),sparse(14,14));



 Tf{8,5} = interval(sparse(14,14),sparse(14,14));



 Tf{8,6} = interval(sparse(14,14),sparse(14,14));



 Tf{8,7} = interval(sparse(14,14),sparse(14,14));



 Tf{8,8} = interval(sparse(14,14),sparse(14,14));



 Tf{8,9} = interval(sparse(14,14),sparse(14,14));



 Tf{8,10} = interval(sparse(14,14),sparse(14,14));



 Tf{8,11} = interval(sparse(14,14),sparse(14,14));



 Tf{8,12} = interval(sparse(14,14),sparse(14,14));



 Tf{8,13} = interval(sparse(14,14),sparse(14,14));



 Tf{8,14} = interval(sparse(14,14),sparse(14,14));



 Tf{9,1} = interval(sparse(14,14),sparse(14,14));



 Tf{9,2} = interval(sparse(14,14),sparse(14,14));



 Tf{9,3} = interval(sparse(14,14),sparse(14,14));



 Tf{9,4} = interval(sparse(14,14),sparse(14,14));



 Tf{9,5} = interval(sparse(14,14),sparse(14,14));



 Tf{9,6} = interval(sparse(14,14),sparse(14,14));



 Tf{9,7} = interval(sparse(14,14),sparse(14,14));



 Tf{9,8} = interval(sparse(14,14),sparse(14,14));



 Tf{9,9} = interval(sparse(14,14),sparse(14,14));



 Tf{9,10} = interval(sparse(14,14),sparse(14,14));



 Tf{9,11} = interval(sparse(14,14),sparse(14,14));



 Tf{9,12} = interval(sparse(14,14),sparse(14,14));



 Tf{9,13} = interval(sparse(14,14),sparse(14,14));



 Tf{9,14} = interval(sparse(14,14),sparse(14,14));



 Tf{10,1} = interval(sparse(14,14),sparse(14,14));



 Tf{10,2} = interval(sparse(14,14),sparse(14,14));



 Tf{10,3} = interval(sparse(14,14),sparse(14,14));



 Tf{10,4} = interval(sparse(14,14),sparse(14,14));



 Tf{10,5} = interval(sparse(14,14),sparse(14,14));



 Tf{10,6} = interval(sparse(14,14),sparse(14,14));



 Tf{10,7} = interval(sparse(14,14),sparse(14,14));



 Tf{10,8} = interval(sparse(14,14),sparse(14,14));



 Tf{10,9} = interval(sparse(14,14),sparse(14,14));



 Tf{10,10} = interval(sparse(14,14),sparse(14,14));



 Tf{10,11} = interval(sparse(14,14),sparse(14,14));



 Tf{10,12} = interval(sparse(14,14),sparse(14,14));



 Tf{10,13} = interval(sparse(14,14),sparse(14,14));



 Tf{10,14} = interval(sparse(14,14),sparse(14,14));

