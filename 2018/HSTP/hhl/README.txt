Software requirements
---------------------------------
Installation of Isabelle:
https://isabelle.in.tum.de/installation.html

Package contents
---------------------------------
1. DCSequents, Assertion.thy: implementation of two assertion languages, i.e. Duration Calculus and First Order Logic;
2. HCSP_Com.thy: the syntax of HCSP (Hybrid Communicating Sequential Process);
3. state.thy, op.thy: the operational semantics of HCSP;
4. HHL.thy: implementation of the Hybrid Hoare Logic (HHL);
5. Sound.thy: the soundness of HHL with respect to the operational semantics of HCSP;
6. CTCS_Conversion.thy: the proof check script of the CTCS case study, containing the specification of the CTCS model;
7. README.txt: information about how to run the CTCS benchmark.

Installation
---------------------------------
No installation is required as long as the 'CTCS_Conversion.thy' is located in the same folder as the HHL Prover, i.e. 
items 1-5 of the package contents.

Running the Benchmark
---------------------------------
Open 'CTCS_Conversion.thy' in Isabelle, and then the proof check will start automatically. When the checking
is completed, the theorem which specifies the safety property of the CTCS case study is successfully proved.
