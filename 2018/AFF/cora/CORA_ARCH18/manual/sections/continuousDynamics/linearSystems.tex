The most basic system dynamics considered in this software package are linear systems of the form 
\begin{equation}\label{eq:linearSystem}
	\dot{x}(t) = A x(t) + B u(t), \quad x(0)\in \mathcal{X}_O \subset \mathbb{R}^n, \quad u(t)\in \mathcal{U} \subset \mathbb{R}^m, \quad A \in \mathbb{R}^{n \times n}, \quad B \in \mathbb{R}^{n \times m}
\end{equation} 
For the computation of reachable sets, we use the equivalent system
\begin{equation} \label{eq:linearSystemSimple}
	\dot{x}(t) = A x(t) + \tilde{u}(t), \quad x(0)\in \mathcal{X}_O \subset \mathbb{R}^n, \quad \tilde{u}(t)\in \tilde{\mathcal{U}} = B \otimes \mathcal{U} \subset \mathbb{R}^n,
\end{equation} 
where $\mathcal{C} \otimes \mathcal{D} = \{C \, D | C\in \mathcal{C}, D \in \mathcal{D} \}$ is the set-based multiplication (one argument can be a singleton).


\subsubsection{Method \texttt{initReach}} \label{sec:linearSystem_initReach}

The method \texttt{initReach} computes the required steps to obtain the reachable set for the first point in time $r$ and the first time interval $[0,r]$ as follows. Given is the linear system in \eqref{eq:linearSystemSimple}. For further computations, we introduce the center of the set of inputs $u_c$ and the deviation from the center of $\tilde{\mathcal{U}}$, $\tilde{\mathcal{U}}_\Delta := \tilde{\mathcal{U}} \oplus (-u_c)$. According to \cite[Section 3.2]{Althoff2010a}, the reachable set for the first time interval $\tau_0 = [0,r]$ is computed as shown in Fig. \ref{fig:linReachOverview}:

\begin{enumerate}
 \item Starting from $\mathcal{X}_O$, compute the set of all solutions $\mathcal{R}^d_h$ for the affine dynamics $\dot{x}(t) = A x(t) + u_c$ at time $r$.
 \item Obtain the convex hull of $\mathcal{X}_O$ and $\mathcal{R}^d_h$ to approximate the reachable set for the first time interval $\tau_0$.
 \item Compute $\mathcal{R}^d(\tau_0)$ by enlarging the convex hull, firstly to bound all affine solutions within $\tau_0$ and secondly to account for the set of uncertain inputs $\tilde{\mathcal{U}}_\Delta$.
\end{enumerate}

\begin{figure}[htb]
    \centering	
    \footnotesize
				
      \psfrag{#t1}[][]{ $\mathcal{X}_O$}									
      \psfrag{#t2}[][]{ $\mathcal{R}^d_h$}			
      \psfrag{#t3}[][]{\shortstack{convex hull of \\
			$\mathcal{X}_O$, $\mathcal{R}^d_h$}}
      \psfrag{#t4}[][]{ $\mathcal{R}^d(\tau_0)$}						
      \psfrag{#t5}[][]{\ding{192}}							
      \psfrag{#t6}[][]{\ding{193}}
      \psfrag{#t7}[][]{\ding{194}}						
      \psfrag{#t8}[][]{ enlargement}					
      \includegraphics[width=0.7\columnwidth]{./figures/ReachsetSteps2.eps}
      \caption{Steps for the computation of an over-approximation of the reachable set for a linear system.}
      \label{fig:linReachOverview}
\end{figure}

The following private functions take care of the required computations:
\begin{itemize}
 \item \texttt{exponential} -- computes an over-approximation of the matrix exponential $e^{Ar}$ based on the Lagrangian remainder as proposed in \cite[Proposition 2]{Althoff2011a}. A more conservative approach previously used \cite[Equation 3.2,3.3]{Althoff2010a}.
 \item \texttt{tie} (\textbf{t}ime \textbf{i}nterval \textbf{e}rror) -- computes the error made by generating the convex hull of reachable sets of points in time for the reachable set of the corresponding time interval as described in \cite[Section 4]{Althoff2011a}. A more conservative approach previously used \cite[Proposition 3.1]{Althoff2010a}, which can only be used in combination with \cite[Equation 3.2,3.3]{Althoff2010a}.
 \item \texttt{inputSolution} -- computes the reachable set due to the input according to the superposition principle of linear systems. The computation is performed as suggested in \cite[Theorem 3.1]{Althoff2010a}. As noted in \cite[Theorem 2]{Althoff2011a}, it is required that the input set is convex. The error term in \cite[Theorem 2]{Althoff2011a} is slightly better, but is computationally more expensive so that the algorithm form \cite[Theorem 3.1]{Althoff2010a} is used.
\end{itemize}


