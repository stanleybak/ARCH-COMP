#!/bin/sh

docker build --tag isaarch:1.0 .
docker create -ti --name dummy isaarch:1.0 bash
docker cp dummy:/home/isabelle/output/document.pdf .
docker rm -f dummy
