function unsafe = inUnsafeSet(R,spec)
% inUnsafeSet - checks if reachable set violates specification
%  (= unsafe set)
% currently only supports zonotopes as set representation of R
%
% Syntax:  
%    unsafe = inUnsafeSet(R,spec)
%
% Inputs:
%    R    - reachable set
%    spec - safety property (unsafe set)
%
% Outputs:
%    unsafe - reachable set in unsafe set
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:        Mark Wetzlinger
% Written:       07-Aug-2019
% Last update:   ---
% Last revision: ---


%------------- BEGIN CODE --------------

if isa(R,'zonotope')
    if any(infimum(interval(spec.normals*R - spec.distances)) <= 0)
        unsafe = true;
    else
        unsafe = false;
    end
else
    error("Currently only zonotope as set representation for" + ...
        " specification check implemented");
end



end

%------------- END OF CODE --------------
