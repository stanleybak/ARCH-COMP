function text = example_hybrid_reach_ARCH20_spacecraft()
% example_hybrid_reach_ARCH20_spacecraft- example of 
% nonlinear reachability analysis from the ARCH19 friendly competition 
% (instance of rendevouz example). The guard intersections are calculated
% with Girard's method
%
% Syntax:  
%    example_hybrid_reach_ARCH20_spacecraft
%
% Inputs:
%    no
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% 
% Author:       Niklas Kochdumper
% Written:      31-May-2018
% Last update:  13-March-2019
% Last revision:---


%------------- BEGIN CODE --------------

% x_1 = v_x
% x_2 = v_y
% x_3 = s_x 
% x_4 = s_y
% x_5 = t



% Parameter ---------------------------------------------------------------

% problem description
R0 = zonotope([[-900;-400;0;0;0],diag([25;25;0;0;0])]);

params.R0 = R0;                                     % initial set
params.startLoc = 1;                                % initial location
params.finalLoc = 0;                                % 0: no final location
params.tFinal = 200;                                % final time



% Reachability Options ----------------------------------------------------

% settings for continuous reachability 
options.timeStepLoc{1} = 2e-1;
options.timeStepLoc{2} = 5e-2;
options.timeStepLoc{3} = 2e-1;

options.alg = 'lin';
options.tensorOrder = 2;
options.taylorTerms = 30;
options.zonotopeOrder = 10;

% settings for hybrid systems
options.guardIntersect = 'nondetGuard';
options.enclose = {'pca'}; 



% Hybrid Automaton --------------------------------------------------------

% specify hybrid automata (automatically converted from SpaceEx)
HA = rendeszvous_nonlinear_passive_nondet();



% Reachability Analysis ---------------------------------------------------

% reachable set computations
tic
HA = reach(HA,params,options);
tComp = toc;



% Verification ------------------------------------------------------------

tic
Rcont = get(HA,'reachableSet');
verificationCollision = 1;
verificationVelocity = 1;
verificationLOS = 1;
spacecraft = interval([-0.1;-0.1],[0.1;0.1]);

% feasible velocity region as polytope
C = [0 0 1 0 0;0 0 -1 0 0;0 0 0 1 0;0 0 0 -1 0;0 0 1 1 0;0 0 1 -1 0;0 0 -1 1 0;0 0 -1 -1 0];
d = [3;3;3;3;4.2;4.2;4.2;4.2];

% line-of-sight as polytope
Cl = [-1 0 0 0 0;tan(pi/6) -1 0 0 0;tan(pi/6) 1 0 0 0];
dl = [100;0;0];

% passive mode -> check if the spacecraft was hit
for i = 1:length(Rcont{3}.OT)    
    temp = interval(Rcont{3}.OT{i}{1});
    if isIntersecting(temp(1:2),spacecraft)
       verificationCollision = 0;
       break;
    end
end

% randezvous attempt -> check if spacecraft inside line-of-sight
for i = 2:length(Rcont{2}.OT)  

    temp = interval(Cl*Rcont{2}.OT{i}{1})-dl;

    if any(supremum(temp) > 0)
       verificationLOS = 0;
       break;
    end
end

% randezvous attempt -> check if velocity inside feasible region
for i = 1:length(Rcont{2}.OT)  

    temp = interval(C*Rcont{2}.OT{i}{1})-d;

    if any(supremum(temp) > 0)
       verificationVelocity = 0;
       break;
    end
end

tVer = toc;
res = verificationCollision & verificationVelocity & verificationLOS;

disp(['specifications verified: ',num2str(verificationCollision & verificationVelocity & verificationLOS)])
disp(['computation time: ',num2str(tVer + tComp)]);




% Visualiztion ------------------------------------------------------------

warning('off','all')
figure 
hold on
box on
dim = [1,2];
color = {'b','m','g'};
plotReachFilled(HA,dim,color);                      % plot reachable set
plotFilled(params.R0,dim,'w','EdgeColor','k');     % plot initial set
xlabel('x');
ylabel('y');

text = ['CORA,SPRE20, ,',num2str(res),',',num2str(tComp+tVer)];



% Auxiliary Functions -----------------------------------------------------

function h = circle(x,y,r)
    hold on
    th = 0:pi/50:2*pi;
    xunit = r * cos(th) + x;
    yunit = r * sin(th) + y;
    fill(xunit,yunit,'y','FaceAlpha',0.6,'EdgeColor','none')



%------------- END OF CODE --------------
