function [Rcont,Rjump] = reach(obj,R0,tStart,options)
% reach - computes the reachable set of the system within a location and
%         determines the intersections with the guard sets
%
% Syntax:  
%    [Rcont,Rjump] = reach(obj,R0,tStart,options)
%
% Inputs:
%    obj - location object
%    R0 - initial reachable set
%    tStart - start time
%    options - struct containing the algorithm settings
%
% Outputs:
%    Rcont - reachable set due to continuous evolution
%    Rjump - list of guard set intersections with the corresponding sets
%
% See also: hybridAutomaton/reach

% Author:       Matthias Althoff, Niklas Kochdumper
% Written:      07-May-2007 
% Last update:  17-August-2007
%               26-March-2008
%               21-April-2009
%               06-July-2009
%               24-July-2009
%               31-July-2009
%               11-August-2010
%               22-October-2010
%               25-October-2010
%               08-August-2011
%               11-September-2013
%               31-July-2016
%               19-August-2016
%               09-December-2019 (NK, integrated singleSetReach)
% Last revision: ---

%------------- BEGIN CODE --------------

    % update factors for the current time step
    for i=1:(options.taylorTerms+1)
        options.factor(i)= options.timeStep^(i)/factorial(i);    
    end

    % set special options
    if ~isfield(options,'compTimePoint')
        options.compTimePoint = 1;
    end
    
    options = set_inputSet(obj.contDynamics,options,'checkOptionsReach');
    
    % compute reachable set for the first time interval
    [Rnext,options] = initReach(obj.contDynamics,R0,options);

    Rcont.OT{1} = Rnext.ti; 
    Rcont.T{1} = Rnext.tp;
    Rcont.R0 = R0;

    t = tStart + options.timeStep;
    iSet = 2; 

    % compute the reachable set until it is completely located outside the
    % invariant set
    inInv = 1;

    while inInv && (t < options.tFinal)

        % compute reachable set for the next time step
        [Rnext, options] = post(obj.contDynamics, Rnext, options);

        Rcont.OT{iSet} = Rnext.ti;     
        Rcont.T{iSet} = Rnext.tp;

        % increment time and set counter
        t = t+options.timeStep;
        iSet = iSet+1; 

        % check if reachable set is still located in invariant
        if iscell(Rnext.ti)
            inInv = 0;
            for i = 1:length(Rnext.ti)
                if isIntersecting(obj.invariant,Rnext.ti{i},'approx')
                   inInv = 1;
                   break;
                end
            end
        else
            inInv = isIntersecting(obj.invariant,Rnext.ti,'approx');
        end
    end

    % determine all guard sets which the reachable set intersects
    [guards,setIndices] = potInt(obj,Rcont.OT,options);

    % compute intersections with the guard sets
    [Rguard,activeGuards,minIndex,maxIndex] = ....
                       guardIntersect(obj,guards,setIndices,Rcont,options);

    % compute reset and get target location
    Rjump = cell(length(Rguard),1);

    for i = 1:length(Rguard)
        iGuard = activeGuards(i);
        Rjump{i,1}.set = reset(obj.transition{iGuard},Rguard{i});  
        Rjump{i,1}.loc = get(obj.transition{iGuard},'target');
        Rjump{i,1}.tMin = tStart + (minIndex(i)-1)*options.timeStep;
        Rjump{i,1}.tMax = tStart + maxIndex(i)*options.timeStep;
    end

    % remove the parts of the reachable sets outside the invariant
    if isfield(options,'intersectInvariant') && ...
       options.intersectInvariant == 1
        Rcont = potOut(obj,Rcont.OT,minIndex,maxIndex,options);
    end

%------------- END OF CODE --------------