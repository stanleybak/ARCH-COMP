import os
import string
import re
from subprocess import Popen, PIPE
from distutils.dir_util import copy_tree

tools = ['ariadne', 'cora', 'dynibex', 'flowstar', 'isabelle-ode-numerics', 'juliareach']

base_dir = os.getcwd()

# create folders for results
if not os.path.isdir('result'):
	os.mkdir('result')

# iterate over all tools
for t in tools:
    print('['+t+']')
    
    if t == 'ariadne':
        string_cmd = 'bash ./measure_all'
    elif t == 'cora':
        string_cmd = 'bash '+t+'/measure_all'
    elif t == 'dynibex':
        string_cmd = 'bash '+t+'/measure_all'
    elif t == 'flowstar':
        string_cmd = 'bash '+t+'/measure_all'
    elif t == 'isabelle-ode-numerics':
        string_cmd = 'bash '+t+'/measure_all'
    elif t == 'juliareach':
        string_cmd = 'bash '+t+'/measure_all'
    else:
        print('ERROR: tool not indicated')
        break
    
    os.chdir(base_dir+'/'+t)

    print('\tCALLING "'+string_cmd+'"')

    # call system command via subprocess for pipe redirection
    p = Popen(string_cmd, stdout=PIPE, stderr=PIPE, stdin=PIPE, shell=True)

    output = str(p.communicate())
    print(output)
    
    if not os.path.isfile('./result/results.csv'):
        print('\tERROR: no results.csv file found in "result" folder')
    else:
        os.chdir(base_dir)
        if not os.path.isdir('result/'+t):
            os.mkdir('result/'+t)
        copy_tree(t+'/result', 'result/'+t)
    
