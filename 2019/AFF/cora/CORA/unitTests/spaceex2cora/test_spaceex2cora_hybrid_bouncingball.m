function pass = test_spaceex2cora_hybrid_bouncingball()
% test_spaceex2cora_hybrid_bouncingball - example for hybrid dynamics
%
% Syntax:  
%    test_spaceex2cora_hybrid_bouncingball
%
% Inputs:
%    no
%
% Outputs:
%    pass - boolean 
% 
% Author:       Matthias Althoff, Raja Judeh
% Written:      27-July-2016
% Last update:  13-September-2018
% Last revision:---


%------------- BEGIN CODE --------------

%% Set options

options.x0 = [1; 0]; %initial state for simulation
options.R0 = zonotope([options.x0, diag([0.05, 0.05])]); %initial state for reachability analysis
options.startLoc = 1; %initial location
options.finalLoc = 0; %0: no final location
options.tStart = 0; %start time
options.tFinal = 1.7; %final time
options.timeStepLoc{1} = 0.05; %time step size for reachable set computation in location 1
options.taylorTerms = 10;
options.zonotopeOrder = 20;
options.polytopeOrder = 10;
options.errorOrder = 2;
options.reductionTechnique = 'girard';
options.isHyperplaneMap = 0;
options.guardIntersect = 'polytope';
options.enclosureEnables = 5; %choose enclosure method(s)
options.originContained = 0;

%% Specify hybrid automation

% Secify linear system of bouncing ball
A = [0 1; 0 0];
B = [0; 0]; % no inputs
c = [0; -9.81]; % constant part of affine dynamics
linSys = linearSys('linearSys',A,B,c); %linear system

% Define large and small distance
dist = 1e3;
eps = 1e-6;
alpha = -0.75; %rebound factor

inv = interval([-2*eps; -dist], [dist; dist]); %invariant
guard = interval([-eps; -dist], [0; -eps]); %guard sets
reset.A = [0, 0; 0, alpha]; reset.b = zeros(2,1); %resets
trans{1} = transition(guard,reset,1,'a','b'); %transitions

% Specify location
loc{1} = location('loc1',1,inv,trans,linSys); 

% Specify hybrid automata
HA = hybridAutomaton(loc); %original hybrid system

%% Convert the spaceEx file into CORA model

spaceex2cora('bball.xml')
HA_SX = bball();

%% Set input:

options.uLoc{1} = 0; % no inputs
options.uLocTrans{1} = 0; % no inputs
options.Uloc{1} = zonotope(0); % no inputs

%% Simulate hybrid automations

HA = simulate(HA, options); %simulate the original file
HA_SX = simulate(HA_SX, options); %simulate the converted file

%% Get the simulation results

simResOriginal = get(HA, 'result');
simResOriginal = simResOriginal.simulation;

simResConverted = get(HA_SX, 'result');
simResConverted = simResConverted.simulation;

%% Compute error between the simulation of the two files

% Note: the number of the channels in simResOriginal exceeds the number of
% channels in simResConverted by one (the first cell is the different one).
% Therefore, we avoid the first cell and compare the rest of the cells. 

numCells = size(simResConverted.x, 2);
simResOriginal.x
simResConverted.x
for cell = 1:numCells
    diff = simResOriginal.x{cell+1} - simResConverted.x{cell};
    error = vecnorm(diff); %has two values: one for each channel
    
    if any(error > 1e-5) 
        disp('Failed Conversion: error = ' + string(error));
        pass = false;
        return
    end 
end

disp('Sucessful Conversion: error = ' + string(error))
pass = true;

%------------- END OF CODE --------------

end
